// Variables that store the data from the api calls and are put into the tables and graphs
var oneHour;
var fourHour;
var day ;
var week ;

var mon ;
var tues ;
var wed ;
var thu ;
var fri ;
var sat ;
var sun ;


//formats the number into GBP
var formatter = new Intl.NumberFormat('en-GB', {
    style: 'currency',
    currency: 'GBP',
});


// Buttons on the page, submit generates the data and update will update the tables
var submitButton = document.getElementById("weekdayInput");
var updateButton = document.getElementById("updateButton");

submitButton.addEventListener('click',formatDate);

// draws the tables and graphs using google charts
updateButton.addEventListener('click',function(){
    var data = new google.visualization.DataTable();
    data.addColumn('string', 'Rental Duration');
    data.addColumn('string', 'Total Taken');

    data.addRows([
        ['1 Hour',formatter.format(oneHour) ],
        ['4 Hours', formatter.format(fourHour)],
        ['1 Day',formatter.format(day)],
        ['1 Week', formatter.format(week)]
    ]);
    var table = new google.visualization.Table(document.getElementById('table_div'));
    table.draw(data, { showRowNumber: false, width: '100%', height: '100%' });


    var dataTwo = new google.visualization.arrayToDataTable([
        ['Day ', 'Total Taken'],
        ["Monday", {v:mon,f:formatter.format(mon)}],
        ["Tuesday", {v:tues,f:formatter.format(tues)}],
        ["Wednesday", {v:wed,f:formatter.format(wed)}],
        ["Thursday", {v:thu,f:formatter.format(thu)}],
        ['Friday', {v:fri,f:formatter.format(fri)}],
        ['Saturday', {v:sat,f:formatter.format(sat)}],
        ['Sunday', {v:sun,f:formatter.format(sun)}]
    ]);

    var options = {
        title: '',
        width: 900,
        legend: { position: 'none' },
        chart: {
            title: '',
            subtitle: ''
        },
        bars: 'horizontal', // Required for Material Bar Charts.
        axes: {
            x: {
                0: { side: 'top', label: 'Total Taken' } // Top x-axis.
            }
        },
        bar: { groupWidth: "90%" }
    };

    var chart = new google.charts.Bar(document.getElementById('top_x_div'));
    chart.draw(dataTwo, options);



    var dataThree = new google.visualization.DataTable();
    dataThree.addColumn('string', 'Day');
    dataThree.addColumn('string', 'Total Taken');

    dataThree.addRows([
        ['Monday', formatter.format(mon)],
        ['Tuesday', formatter.format(tues)],
        ['Wednesday', formatter.format(wed)],
        ['Thursday', formatter.format(thu)],
        ['Friday', formatter.format(fri)],
        ['Saturday', formatter.format(sat)],
        ['Sunday', formatter.format(sun)]
    ]);

    var table2 = new google.visualization.Table(document.getElementById('table2_div'));

    table2.draw(dataThree, { showRowNumber: false, width: '100%', height: '100%' });


    var dataFour = new google.visualization.arrayToDataTable([
        ['Rental Type ', 'Total Taken'],
        ["1 Hour", {v:oneHour,f:formatter.format(oneHour)}],
        ["4 Hours", {v:fourHour,f:formatter.format(fourHour)}],
        ["1 Day", {v:day,f:formatter.format(day)}],
        ["1 Week", {v:week,f:formatter.format(week)}]
    ]);

    var optionsTwo = {
        title: '',
        width: 900,
        legend: { position: 'none' },
        chart: {
            title: '',
            subtitle: ''
        },
        bars: 'horizontal', // Required for Material Bar Charts.
        axes: {
            x: {
                0: { side: 'top', label: 'Total Taken' } // Top x-axis.
            }
        },
        bar: { groupWidth: "90%" }
    };

    var chartTwo = new google.charts.Bar(document.getElementById('top_x_div_weekly'));
    chartTwo.draw(dataFour, optionsTwo);
});

//loads the packages needed for the charts
google.charts.load('current', { 'packages': ['bar'] });

google.charts.load('current', { 'packages': ['table'] });


//formats the data entered and calls the api functions
async function formatDate(){
    var inputDate = document.getElementById("camp-week").valueAsDate.toString();
    const splitDate = inputDate.split(' ');
    splitDate.shift();
    splitDate.splice(3,5);
    console.log(splitDate);

    var newString = splitDate[2] + "-";
    if (splitDate[0] == "Jan") {
        newString = newString + "01-";
    }
    else if (splitDate[0] == "Feb") {
        newString = newString + "02-";
    }
    else if (splitDate[0] == "Mar") {
        newString = newString + "03-";
    }
    else if (splitDate[0] == "Apr") {
        newString = newString + "04-";
    }
    else if (splitDate[0] == "May") {
        newString = newString + "05-";
    }
    else if (splitDate[0] == "Jun") {
        newString = newString + "06-";
    }
    else if (splitDate[0] == "Jul") {
        newString = newString + "07-";
    }
    else if (splitDate[0] == "Aug") {
        newString = newString + "08-";
    }
    else if (splitDate[0] == "Sep") {
        newString = newString + "09-";
    }
    else if (splitDate[0] == "Oct") {
        newString = newString + "10-";
    }
    else if (splitDate[0] == "Nov") {
        newString = newString + "11-";
    }
    else if (splitDate[0] == "Dec") {
        newString = newString + "12-";
    }

    newString = newString + splitDate[1];

    function convert(date, i){
        let d = new Date(newString);
        d.setUTCDate(d.getUTCDate() + 1 + i);
        return d.toISOString().substr(0,10);
    }

    const days = [newString];
    for (let i = 0; i < 6; i++) {


        days.push(convert(days[i],i));

    }

    //call api for each value and add to table

    const totals = [];

    for (let j = 0; j < 7; j++) {
        let amount = await getDaily(days[j]);

        if (amount == null) {
            amount = 0;
        }

        if (Number.isNaN(amount)){
            amount = 0;
        }
        console.log(amount);
        totals.push(amount);

    }

    console.log(totals);
    mon = totals[0];
    tues = totals[1];
    wed = totals[2];
    thu = totals[3];
    fri = totals[4];
    sat = totals[5];
    sun = totals[6];


    //Api calls for Rental types

    const totalsTwo = [];

    for (let k = 1; k < 5; k++) {
        let amount = await getRental(days[0],days[6],k);

        if (amount == null) {
            amount = 0;
        }

        if (Number.isNaN(amount)){
            amount = 0;
        }
        console.log(amount);
        totalsTwo.push(amount);
    }

    console.log(totalsTwo);
    oneHour = totalsTwo[0];
    fourHour = totalsTwo[1];
    day = totalsTwo[2];
    week = totalsTwo[3];
}

//calls the api which gets the data from the database
async function getRental(startDate, endDate, id){
    var jsonOutput;

    var url = "https://e-scooter-api.herokuapp.com/getRentalTotals/";
    url = url + ('"')+ (startDate)+ ('"/"') + (endDate) + ('"/') + id;

    console.log(url)
    const response = await fetch(url);
    const json = await response.json();

    let finalCost = json[0]["sum(FinalCost)"];

    return finalCost;
}

//calls the api which gets the data from the database
async function getDaily(date) {

    var jsonOutput;

    var url = "https://e-scooter-api.herokuapp.com/getDailyTotals/";
    url = url + ('"');
    url= url + (date);
    url= url + ('"')

    const response = await fetch(url);
    const json = await response.json();

    let finalCost = json[0]["SUM(FinalCost)"];

    return finalCost;

}