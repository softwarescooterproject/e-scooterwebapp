// Initialize and add the map
function initMap() {
    // The location of Uluru
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 4,
        center: {lat:53.802126,lng:-1.543302},
    });
    const infoWindow = new google.maps.InfoWindow({
        content: "",
        disableAutoPan: true,
    });
    // The marker, positioned at Uluru
    marker.addListener("click", () => {
        infoWindow.setContent(label);
        infoWindow.open(map, marker);
    });
}

const locations = [
    { lat: 53.802126, lng:-1.543302 },
    { lat: 53.794829, lng: -1.547601 },
    { lat: 53.796628, lng: -1.5445 },
    { lat: 53.802911, lng: -1.551571 },
    {lat:53.804181,lng:-1.553221}
];
window.initMap = initMap;