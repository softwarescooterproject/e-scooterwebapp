1) Optional - Ensure that you have intellij installed.
2) Install this specific java jdk -> https://www.oracle.com/uk/java/technologies/javase/jdk11-archive-downloads.html The play framework does not support java version beyond 11 and will not work with other jdks.
3) Install the build tool SBT -> https://www.scala-sbt.org/
4) Optional - Add Scala plugin to intellij, at the start-up screen, there will be a "configure" icon at the bottom right, press this and then "plugins".
5) Open terminal within the project directory and enter command "sbt run"


The website has been deployed online: https://escooter-web-app.herokuapp.com/

If you want to login: 

Manager: admin@admin.com admin123
Employee: emp@emp.com   test
