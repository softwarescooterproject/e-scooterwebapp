import com.fasterxml.jackson.databind.JsonNode;
import controllers.ApplicationController;
import controllers.AssetsFinder;
import controllers.HomeController;
import controllers.ResultPages;
import models.Scooter;
import org.json.simple.JSONObject;
import org.junit.Test;
import play.libs.ws.*;
import play.mvc.Result;

import javax.inject.Inject;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static play.test.Helpers.contentAsString;

public class UnitTest{

    private WSClient ws;

    @Inject
    AssetsFinder assetsFinder;

    @Test
    public void simpleCheck() {
        int a = 1 + 1;
        assertThat(a).isEqualTo(2);
    }
    @Test
    public void apiConnection() throws InterruptedException, IOException, URISyntaxException {
        HttpRequest apiRequest = HttpRequest.newBuilder().uri(new URI("https://e-scooter-api.herokuapp.com/")).GET()
                .build();
        int httpResponse = HttpClient.newHttpClient()
                .send(apiRequest, HttpResponse.BodyHandlers.ofString()).statusCode();
        assertThat(httpResponse).isEqualTo(200);
    }

}




