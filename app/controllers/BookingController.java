package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Customer;
import models.Scooter;
import play.data.Form;
import play.data.FormFactory;
import play.i18n.MessagesApi;
import play.libs.Json;
import play.libs.ws.*;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

public class BookingController extends Controller implements WSBodyReadables, WSBodyWritables {

    public static final String EMPLOYEE_ACCESS = "employeeAccess";
    public static final String MANAGER_ACCESS = "managerAccess";
    public static final String VIEW_ACTIVE_BOOKING = "/viewActiveBooking/";
    public static final String api_prefix = "https://e-scooter-api.herokuapp.com";
    public static final String GET_ALL_SCOOTER_DETAILS = "/getScooterDetails/all";
    //inject as more reusable and reduced dependencies
    @Inject
    FormFactory formFactory;

    @Inject
    MessagesApi messagesApi;

    @Inject
    WSClient ws;

    @Inject
    AssetsFinder assetsFinder;


    Scooter selectedScooter;

    @Inject
    public BookingController(AssetsFinder assetsFinder, WSClient ws) {
        this.ws = ws;
        this.assetsFinder = assetsFinder;
    }

    public String locationIdToString(String locationId) {
        return getString(locationId);
    }


    //retrieves booking details

    public Result bookingDetails(Http.Request request) throws ExecutionException, InterruptedException {
        List<Scooter> scooterList = getAvailableScooterList();
        List<Scooter> availableScooters = getAvailableScooterList();
        if (request.session().get(EMPLOYEE_ACCESS).isPresent() || request.session().get(MANAGER_ACCESS).isPresent()) {
            Form<Customer> customerForm = formFactory.form(Customer.class);
            return ok(views.html.booking.render(scooterList, customerForm, availableScooters, messagesApi.preferred(request), null, assetsFinder));
        } else {
            return (redirect(routes.HomeController.index()));
        }
    }

    //posts an unregistered booking to the database
    public Result bookingPost(Http.Request request) throws ExecutionException, InterruptedException {
        Form<Customer> customerForm = formFactory.form(Customer.class).bindFromRequest(request);
        Customer customer = customerForm.get();


        if (scooterBookingAttempt(customer)) {
            return redirect("/successfulBooking");

        } else {
            return redirect("/unSuccesfulBooking");
        }

    }

    //allows to view current bookings

    public Result viewBooking(Http.Request request) throws ExecutionException, InterruptedException {
        List<Scooter> scooterList = getUnvailableScooterList();
        List<Scooter> unAvailableScooters = getUnvailableScooterList();
        if (request.session().get(EMPLOYEE_ACCESS).isPresent() || request.session().get(MANAGER_ACCESS).isPresent()) {
            Form<Customer> customerForm = formFactory.form(Customer.class);
            return ok(views.html.viewbooking.render(
                    scooterList, customerForm, unAvailableScooters, messagesApi.preferred(request), null, assetsFinder));
        } else {
            return (redirect(routes.HomeController.index()));
        }
    }

    //views selected booking
    public Result viewBookingPost(Http.Request request) {
        Form<Customer> customerForm = formFactory.form(Customer.class).bindFromRequest(request);
        Customer customer = customerForm.get();
        String currentID = customer.scooterID;
        String url = VIEW_ACTIVE_BOOKING + currentID;

        if (request.session().get(EMPLOYEE_ACCESS).isPresent() || request.session().get(MANAGER_ACCESS).isPresent()) {
            return redirect(url);
        } else {
            return (redirect(routes.HomeController.index()));
        }
    }

    //gets where the booking scooter is as well as the cost of it
    public Result viewActiveBooking(Http.Request request, Long id) throws ExecutionException, InterruptedException {
        if (request.session().get(EMPLOYEE_ACCESS).isPresent() || request.session().get(MANAGER_ACCESS).isPresent()) {
            Scooter currentScooter = null;
            JsonNode jsonData = apiCall(api_prefix + "/getCurrentSessions");
            for (int i = 0; i < jsonData.size(); i++) {
                if (jsonData.get(i).get("ScooterID").toString().equals(Long.toString(id))) {
                    String CurrentLocation = locationIdToString(jsonData.get(i).get("ScooterLocation").toString());
                    String ScooterID = jsonData.get(i).get("ScooterID").toString();
                    String SessionID = jsonData.get(i).get("SessionID").toString();
                    int costID = Integer.parseInt(jsonData.get(i).get("CostID").toString());
                    currentScooter = new Scooter("No", CurrentLocation, ScooterID, SessionID);
                    currentScooter.costID = costID;
                }
            }
            assert currentScooter != null;
            selectedScooter = currentScooter;
            List<String> extendSessionOptions = currentScooter.extensionOptions();
            return ok(views.html.viewActiveBooking.render(currentScooter, extendSessionOptions, assetsFinder));

        } else {
            return (redirect(routes.HomeController.index()));
        }
    }

    //allows for the scooter booking to be deleted
    public Result deleteBooking() throws ExecutionException, InterruptedException {
        int currentLocation = selectedScooter.StringtoLocationID(selectedScooter.currentLocation);
        JsonNode bookingJSON = Json.newObject().put("Availability", 1).put("CurrentLocation", currentLocation).put("ScooterID", Integer.parseInt(selectedScooter.getScooterID()));
        WSRequest deleteScooter = ws.url(api_prefix + "/returnScooter").addHeader("Content-Type", "application/json");
        CompletionStage<WSResponse> responsePromise = deleteScooter.put(bookingJSON);
        CompletionStage<Integer> httpCodePromise = responsePromise.thenApply(WSResponse::getStatus);

        int httpCodeResponse = httpCodePromise.toCompletableFuture().get();

        if (httpCodeResponse == 200) {
            return redirect("/viewBooking");
        } else {
            return (redirect(routes.HomeController.index()));
        }
    }

    //allows to extend current bookings
    public Result extendBookingOptions(String CostID) throws ExecutionException, InterruptedException {


        if (Objects.equals(CostID, "4 hours")) {
            selectedScooter.costID = 2;
        }
        if (Objects.equals(CostID, "1 day")) {
            selectedScooter.costID = 3;
        }
        if (Objects.equals(CostID, "1 week")) {
            selectedScooter.costID = 4;
        }
        JsonNode extendJson = Json.newObject().put("ScooterID", selectedScooter.scooterID).put("CostID", selectedScooter.costID);

        WSRequest extendScooter = ws.url(api_prefix + "/employeeExtendSession").addHeader("Content-Type", "application/json");
        CompletionStage<WSResponse> responsePromise = extendScooter.put(extendJson);
        CompletionStage<Integer> httpCodePromise = responsePromise.thenApply(WSResponse::getStatus);

        int httpCodeResponse = httpCodePromise.toCompletableFuture().get();

        if (httpCodeResponse == 200) {
            return redirect("/viewBooking");
        } else {
            return (redirect(routes.HomeController.index()));
        }


    }

    //makes sure the api returns 200 for when scooter is booked
    public boolean scooterBookingAttempt(Customer customer) throws InterruptedException, ExecutionException {

        JsonNode json = Json.newObject().put("CustomerID", customer.getCustomerID()).put("ScooterID", customer.getScooterID()).put("CostID", customer.getCostID());
        WSRequest userRequest = ws.url(api_prefix + "/addNewSession");
        WSRequest complexRequest = userRequest.addHeader("Content-Type", "application/json");
        CompletionStage<WSResponse> responsePromise = complexRequest.post(json);
        CompletionStage<Integer> httpCodePromise = responsePromise.thenApply(WSResponse::getStatus);

        int httpCodeResponse = httpCodePromise.toCompletableFuture().get();
        return httpCodeResponse == 200;

    }

    //gets available scooters
    public List<Scooter> getAvailableScooterList() throws InterruptedException, ExecutionException {
        JsonNode jsonData = apiCall(api_prefix + GET_ALL_SCOOTER_DETAILS);
        List<Scooter> scooterList = new ArrayList<>();
        for (int i = 0; i < jsonData.size(); i++) {
            if (jsonData.get(i).get("Available").toString().equals("1")) {
                String Available = jsonData.get(i).get("Available").toString();
                String CurrentLocation = locationIdToString(jsonData.get(i).get("CurrentLocation").toString());
                String ScooterID = jsonData.get(i).get("ScooterID").toString();
                Scooter temp = new Scooter(Available, CurrentLocation, ScooterID);
                scooterList.add(temp);
            }
        }
        return scooterList;
    }

    // gets unavailable scooters
    public List<Scooter> getUnvailableScooterList() throws InterruptedException, ExecutionException {
        JsonNode jsonData = apiCall(api_prefix + GET_ALL_SCOOTER_DETAILS);
        List<Scooter> scooterList = new ArrayList<>();
        for (int i = 0; i < jsonData.size(); i++) {
            if (jsonData.get(i).get("Available").toString().equals("0")) {
                String Available = jsonData.get(i).get("Available").toString();
                String CurrentLocation = locationIdToString(jsonData.get(i).get("CurrentLocation").toString());
                String ScooterID = jsonData.get(i).get("ScooterID").toString();
                Scooter temp = new Scooter(Available, CurrentLocation, ScooterID);
                scooterList.add(temp);
            }
        }
        return scooterList;
    }

    //changes location ID to location strings
    static String getString(String locationId) {
        if (locationId.equals("1")) {
            return "Trinity centre";
        }
        if (locationId.equals("2")) {
            return "Train station";
        }
        if (locationId.equals("3")) {
            return "Merrion centre";
        }
        if (locationId.equals("4")) {
            return "LRI hospital";
        }
        if (locationId.equals("5")) {
            return "UoL Edge sports centre";
        }
        return "";
    }


    private JsonNode apiCall(String URL) throws InterruptedException, ExecutionException {
        WSRequest scooterRequest = ws.url(URL);
        CompletionStage<? extends WSResponse> responsePromise = scooterRequest.get();
        CompletionStage<JsonNode> jsonPromise = responsePromise.thenApply(r -> r.getBody(json()));
        return jsonPromise.toCompletableFuture().get();
    }


}
