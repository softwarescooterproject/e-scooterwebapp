package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Admin;
import play.data.Form;
import play.data.FormFactory;
import play.i18n.MessagesApi;
import play.libs.Json;
import play.libs.ws.*;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class LogInController extends Controller implements WSBodyReadables, WSBodyWritables {
    @Inject
    FormFactory formFactory;

    @Inject
    MessagesApi messagesApi;

    @Inject
    WSClient ws;

    @Inject
    AssetsFinder assetsFinder;


    public LogInController() {

    }

    //gets user data from login
    public Result logInAttempt(Http.Request request) {
        Form<Admin> userForm = formFactory.form(Admin.class);
        return ok(views.html.logIn.render(
                userForm, messagesApi.preferred(request), null, assetsFinder));
    }


    //checks if login details work
    public Result logInPost(Http.Request request) throws ExecutionException, InterruptedException {
        Form<Admin> userForm = formFactory.form(Admin.class).bindFromRequest(request);
        if (userForm.hasErrors()) {
            String errors = userForm.errors().toString();
            StringBuilder errorMessage = new StringBuilder();
            if (errors.contains("email") && !errors.contains("password")) {
                errorMessage.append("Please enter a valid email");
            } else if (errors.contains("password") && !errors.contains("email")) {
                errorMessage.append("Please enter a valid password");
            } else {
                errorMessage.append("Please enter a valid email and password");
            }
            return badRequest(views.html.logIn.render(userForm, messagesApi.preferred(request), errorMessage.toString(), assetsFinder));
        }

        Admin admin = userForm.get();

        if (authenticateAPI(admin)) {
            if (admin.isManager()) {
                return redirect(routes.SessionController.session()).addingToSession(request, "managerAccess", "1");
            } else {
                return redirect(routes.SessionController.session()).addingToSession(request, "employeeAccess", "1");
            }
        } else {
            return badRequest(views.html.logIn.render(userForm, messagesApi.preferred(request), "Your email and password combination is incorrect", assetsFinder));
        }

    }

    //asks api for confirmation of login details
    public boolean authenticateAPI(Admin admin) throws InterruptedException, ExecutionException {

        JsonNode json = Json.newObject().put("Email", admin.getEmail()).put("Password", admin.getPassword());

        WSRequest userRequest = ws.url("https://e-scooter-api.herokuapp.com/adminLogin").addHeader("Content-Type", "application/json");
        CompletableFuture<WSResponse> responsePromise = (CompletableFuture<WSResponse>) userRequest.post(json);
        CompletableFuture<JsonNode> jsonPromise = responsePromise.thenApply(r -> r.getBody(json()));
        CompletableFuture<Integer> httpCodePromise = responsePromise.thenApply(WSResponse::getStatus);

        int httpCode = httpCodePromise.toCompletableFuture().get();
        JsonNode jsonData = jsonPromise.toCompletableFuture().get();

        if (httpCode != 200) {
            return false;
        }
        int access = jsonData.get(0).get("Full_Access").asInt();
        if (access == 1) {
            admin.setManager(true);
        }

        return true;

    }


}
