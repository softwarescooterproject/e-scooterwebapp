package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.*;
import play.data.Form;
import play.data.FormFactory;
import play.i18n.MessagesApi;
import play.libs.ws.*;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

import static controllers.BookingController.getString;


public class ApplicationController extends Controller implements WSBodyReadables, WSBodyWritables {

    //string constants
    public static final String apiPrefix = "https://e-scooter-api.herokuapp.com";
    public static final String EMPLOYEE_ACCESS = "employeeAccess";
    public static final String MANAGER_ACCESS = "managerAccess";
    public static final String AVAILABLE = "Available";
    public static final String SCOOTER_ID = "ScooterID";
    public static final String CURRENT_LOCATION = "CurrentLocation";
    public static final String ALL_FEEDBACK = "/getFeedback/all";
    public static final String ALL_SCOOTER_PRICING = "/getScooterPricing/all";
    private final WSClient ws;

    //inject as more reusable and reduced dependencies
    @Inject
    FormFactory formFactory;

    @Inject
    MessagesApi messagesApi;

    private final AssetsFinder assetsFinder;


    @Inject
    public ApplicationController(AssetsFinder assetsFinder, WSClient ws) {
        this.ws = ws;
        this.assetsFinder = assetsFinder;
    }

    //gets json from api
    private JsonNode apiCall(String URL) throws InterruptedException, ExecutionException {
        //completion stages to ensure calls go through
        WSRequest scooterRequest = ws.url(URL);
        CompletionStage<? extends WSResponse> responsePromise = scooterRequest.get();
        CompletionStage<JsonNode> jsonPromise = responsePromise.thenApply(r -> r.getBody(json()));
        return jsonPromise.toCompletableFuture().get();
    }

    //gets all available scooters
    public List<Scooter> getScooterList() throws ExecutionException, InterruptedException {
        JsonNode jsonData = apiCall(apiPrefix + "/getScooterDetails/all");
        List<Scooter> scooterList = new ArrayList<>();
        for (int i = 0; i < jsonData.size(); i++) {
            String Available = jsonData.get(i).get(AVAILABLE).toString();
            String CurrentLocation = locationIdToString(jsonData.get(i).get(CURRENT_LOCATION).toString());
            String ScooterID = jsonData.get(i).get(SCOOTER_ID).toString();

            Scooter temp = new Scooter(Available, CurrentLocation, ScooterID);

            scooterList.add(temp);
        }
        return scooterList;
    }

    //gets all feedback
    public List<Feedback> getFeedbackList() throws ExecutionException, InterruptedException {
        JsonNode jsonData = apiCall(apiPrefix + ALL_FEEDBACK);

        List<Feedback> feedbackList = new ArrayList<>();
        for (int feedbackLevel = 1; feedbackLevel < 5; feedbackLevel++) {
            for (int i = 0; i < jsonData.size(); i++) {
                String FeedbackContents = jsonData.get(i).get("Feedback Contents").toString();
                String FeedbackID = (jsonData.get(i).get("Feedback ID").toString());
                String Priority = (jsonData.get(i).get("Priority").toString());
                String UserID = (jsonData.get(i).get("User ID").toString());

                if (Integer.parseInt(Priority) == feedbackLevel) {
                    Feedback temp = new Feedback(FeedbackContents, Integer.parseInt(FeedbackID), Integer.parseInt(Priority), Integer.parseInt(UserID));
                    feedbackList.add(temp);
                }
            }
        }
        return feedbackList;
    }

    //get prices
    public List<Prices> getPriceList() throws ExecutionException, InterruptedException {
        JsonNode jsonData = apiCall(apiPrefix + ALL_SCOOTER_PRICING);

        List<Prices> PricesList = new ArrayList<>();
        for (int i = 0; i < jsonData.size(); i++) {

            String CostID = jsonData.get(i).get("CostID").toString();
            String Duration = jsonData.get(i).get("Duration(mins)").toString();
            String Price = jsonData.get(i).get("Price").toString();

            Prices temp = new Prices(Integer.parseInt(CostID), Integer.parseInt(Duration), Integer.parseInt(Price));

            PricesList.add(temp);
        }

        return PricesList;
    }

    //gets all scooters
    public Result scooterInformation(Http.Request request) throws ExecutionException, InterruptedException {
        if (request.session().get(MANAGER_ACCESS).isPresent() || request.session().get(EMPLOYEE_ACCESS).isPresent()) {
            List<Scooter> scooterList = getScooterList();
            return ok(
                    views.html.scooterInformation.render(scooterList, assetsFinder));
        } else {
            return redirect("/");
        }
    }

    //displays scooters on booking page
    public Result scooterBooking(Http.Request request) throws InterruptedException, ExecutionException {
        if (request.session().get(MANAGER_ACCESS).isPresent() || request.session().get(EMPLOYEE_ACCESS).isPresent()) {
            List<Scooter> scooterList = getScooterList();
            return ok(
                    views.html.scooterBooking.render(scooterList, assetsFinder));
        } else {
            return redirect("/");
        }
    }

    //gets feedback and renders to web page
    public Result feedback(Http.Request request) throws InterruptedException, ExecutionException {
        if (request.session().get(MANAGER_ACCESS).isPresent()) {
            Form<FeedbackChanger> feedbackForm = formFactory.form(FeedbackChanger.class);
            List<Feedback> feedbackList = getFeedbackList();
            return ok(
                    views.html.feedback.render(feedbackForm, feedbackList, assetsFinder, messagesApi.preferred(request), null));
        } else if (request.session().get(EMPLOYEE_ACCESS).isPresent()) {
            return redirect(routes.ResultPages.badPermissions());
        } else {
            return redirect("/");
        }
    }

    //updates feedback
    public Result feedbackPost(Http.Request request, Long id) throws InterruptedException {
        Form<FeedbackChanger> feedbackForm = formFactory.form(FeedbackChanger.class).bindFromRequest(request);
        FeedbackChanger feed = feedbackForm.get();
        int newPriority = feed.getNewPriority();

        String URL = apiPrefix + "/changeFeedbackPriority/" + id + "/" + newPriority;
        WSRequest scooterRequest = ws.url(URL);
        scooterRequest.put("");

        Thread.sleep(200L);


        return redirect("/feedback");
    }

    //gets prices and renders to page
    public Result prices(Http.Request request) throws InterruptedException, ExecutionException {
        if (request.session().get(EMPLOYEE_ACCESS).isPresent() || request.session().get(MANAGER_ACCESS).isPresent()) {
            List<Prices> pricesList = getPriceList();
            Form<PriceChanger> pricesForm = formFactory.form(PriceChanger.class);
            return ok(
                    views.html.prices.render(pricesForm, pricesList, assetsFinder, messagesApi.preferred(request), null));
        } else {
            return redirect("/");
        }
    }

    //allows us to upload new prices
    public Result pricesPost(Http.Request request, Long id) throws InterruptedException {
        if (request.session().get(MANAGER_ACCESS).isPresent()) {
            Form<PriceChanger> priceForm = formFactory.form(PriceChanger.class).bindFromRequest(request);
            PriceChanger p = priceForm.get();
            int newPrice = p.getNewPrice();

            //make sure number is not zero
            if(newPrice <=0){
                return redirect(routes.ResultPages.badPermissions());
            }

            String URL = apiPrefix + "/changePrice/" + id + "/" + newPrice;
            WSRequest scooterRequest = ws.url(URL);
            scooterRequest.put("");

            Thread.sleep(200L);

            return redirect("/prices");
        } else {
            return (redirect(routes.ResultPages.badPermissions()));
        }
    }

    public Result analytics(Http.Request request) {
        if (request.session().get(MANAGER_ACCESS).isPresent()) {
            return ok(
                    views.html.analytics.render(assetsFinder));
        } else if (request.session().get(EMPLOYEE_ACCESS).isPresent()) {
            return redirect(routes.ResultPages.badPermissions());
        } else {
            return redirect("/");
        }
    }

    public Result welcome(String message) {
        return ok(
                views.html.welcome.render(message));
    }

    public String locationIdToString(String locationId) {
        return getString(locationId);
    }
}
