package controllers;

import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

public class SessionController extends Controller {

    //depending on type of user, gives them a cookie with managerial or employee access
    public Result session(Http.Request request) {

        if (request.session().get("managerAccess").isPresent()) {
            return redirect("/index");
        } else if (request.session().get("employeeAccess").isPresent()) {
            return redirect("/index");
        } else {
            return redirect(routes.ResultPages.notLoggedIn());
        }
    }

    //destroys cookie
    public Result logOut() {
        return redirect("/login").withNewSession();
    }
}
