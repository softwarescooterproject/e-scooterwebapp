package controllers;

import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import views.html.index;

import javax.inject.Inject;

public class HomeController extends Controller {

    private final AssetsFinder assetsFinder;


    @Inject
    public HomeController(AssetsFinder assetsFinder) {
        this.assetsFinder = assetsFinder;
    }

    //home page
    public Result index(Http.Request request) {
        if (request.session().get("managerAccess").isPresent() || request.session().get("employeeAccess").isPresent()) {
            return ok(
                    index.render(
                            "E-scooter.",
                            assetsFinder
                    ));
        } else {
            return redirect("/login");
        }


    }
}
