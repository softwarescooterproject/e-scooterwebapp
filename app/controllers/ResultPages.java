package controllers;

import play.mvc.Controller;
import play.mvc.Result;

import javax.inject.Inject;

public class ResultPages extends Controller {
    //class to redirect to different results
    @Inject
    AssetsFinder assetsFinder;

    public Result badPermissions() {
        return ok(
                views.html.noPermission.render(assetsFinder));
    }

    public Result notLoggedIn() {
        return ok(
                views.html.notLoggedIn.render(assetsFinder));
    }

    public Result succesfulBooking() {
        return ok(
                views.html.successfulBooking.render(assetsFinder));
    }

    public Result unSuccesfulBooking() {
        return ok(
                views.html.unSuccesfulBooking.render(assetsFinder));
    }
}
