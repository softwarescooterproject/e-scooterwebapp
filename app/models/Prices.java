package models;

public class Prices {

    public int CostID;
    public int Duration;
    public int Price;

    public Prices(int CostID, int Duration, int Price) {
        this.CostID = CostID;
        this.Duration = Duration;
        this.Price = Price;
    }

    public int getCostID() {
        return CostID;
    }

    public void setCostID(int costID) {
        CostID = costID;
    }

    public int getDuration() {
        return Duration;
    }

    public void setDuration(int duration) {
        Duration = duration;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        Price = price;
    }
}
