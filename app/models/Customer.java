package models;

import play.data.validation.Constraints;

import javax.validation.Constraint;

public class Customer {
    //customer ID's to start from 1000 +


    public String firstName;

    public String secondName;

    public String scooterID;

    public String cardNumber;

    public String expiryDate;
    public String cvc;
    public String customerEmail;

    public String costID = "-1";

    //allows us to book unregistered users
    public final String customerID = "72";

    public Customer() {
    }


    public Customer(String firstName, String secondName, String scooterID, String costID, String cardNumber, String expiryDate, String cvc, String customerEmail) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.scooterID = scooterID;
        this.costID = costID;
        this.cardNumber = cardNumber;
        this.expiryDate = expiryDate;
        this.cvc = cvc;
        this.customerEmail = customerEmail;
    }

    public String getCustomerID() {
        return customerID;
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getScooterID() {
        return scooterID;
    }

    public void setScooterID(String scooterID) {
        this.scooterID = scooterID;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getCvc() {
        return cvc;
    }

    public void setCvc(String cvc) {
        this.cvc = cvc;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCostID() {
        return costID;
    }

    public void setCostID(String costID) {
        this.costID = costID;
    }
}
