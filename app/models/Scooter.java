package models;

import java.util.ArrayList;
import java.util.List;

public class Scooter {
    public String available;
    public String currentLocation;
    public String scooterID;
    public String sessionID;

    public int costID;

    public Scooter(String available, String currentLocation, String scooterID) {
        this.available = available;
        this.currentLocation = currentLocation;
        this.scooterID = scooterID;
    }

    public Scooter(String available, String currentLocation, String scooterID, String sessionID) {
        this.available = available;
        this.currentLocation = currentLocation;
        this.scooterID = scooterID;
        this.sessionID = sessionID;
    }

    public Scooter() {
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getScooterID() {
        return scooterID;
    }

    public void setScooterID(String scooterID) {
        this.scooterID = scooterID;
    }

    public int StringtoLocationID(String name) {

        if (name.equals("Trinity centre")) {
            return 1;
        }
        if (name.equals("Train station")) {
            return 2;
        }
        if (name.equals("Merrion centre")) {
            return 3;
        }
        if (name.equals("LRI hospital")) {
            return 4;
        }
        if (name.equals("UoL Edge sports centre")) {
            return 5;
        }
        return 0;
    }

    public List<String> extensionOptions() {
        List<String> extensions = new ArrayList<>();
        if (costID == 1) {
            extensions.add("4 hours");
            extensions.add("1 day");
            extensions.add("1 week");
        } else if (costID == 2) {
            extensions.add("1 day");
            extensions.add("1 week");
        } else if (costID == 3) {
            extensions.add("1 week");
        } else if (costID == 4) {
            extensions.add("No extension available");
        }
        return extensions;
    }
}
