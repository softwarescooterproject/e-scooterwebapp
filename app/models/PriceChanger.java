package models;

public class PriceChanger {

    public int priceId;
    public int newPrice;

    public PriceChanger() {
    }

    public PriceChanger(int PriceId, int newPrice) {

        this.priceId = PriceId;
        this.newPrice = newPrice;

    }

    public int getPriceId() {
        return priceId;
    }

    public void setPriceId(int priceId) {
        this.priceId = priceId;
    }

    public int getNewPrice() {

            return newPrice;
    }

    public void setNewPrice(int newPrice) {
        this.newPrice = newPrice;
    }

}