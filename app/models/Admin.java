package models;

import play.data.validation.Constraints;


public class Admin {


    @Constraints.Required
    @Constraints.Email
    public String email;

    @Constraints.Required
    public String password;


    //true = 1 -> manager
    public boolean manager;

    public Admin() {
    }

    public Admin(String email, String password, boolean manager) {
        this.email = email;
        this.password = password;
        this.manager = manager;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public void setPassword(String password) {

        this.password = password;
    }

    public boolean isManager() {
        return manager;
    }

    public void setManager(boolean manager) {
        this.manager = manager;
    }

    public String getPassword() {
        return password;
    }


}

