package models;

public class Feedback {
    public String FeedbackContents;
    public int FeedbackID;
    public int Priority;
    public int UserID;

    public Feedback() {
    }

    public Feedback(String FeedbackContents, int FeedbackID, int Priority, int UserID) {
        this.FeedbackContents = FeedbackContents;
        this.FeedbackID = FeedbackID;
        this.Priority = Priority;
        this.UserID = UserID;
    }

    public String getFeedbackContents() {
        return FeedbackContents;
    }

    public void setFeedbackContents(String feedbackContents) {
        FeedbackContents = feedbackContents;
    }

    public int getFeedbackID() {
        return FeedbackID;
    }

    public void setFeedbackID(int feedbackID) {
        FeedbackID = feedbackID;
    }

    public int getPriority() {
        return Priority;
    }

    public void setPriority(int priority) {
        Priority = priority;
    }

    public int getUserID() {
        return UserID;
    }

    public void setUserID(int userID) {
        UserID = userID;
    }
}
