package models;

public class FeedbackChanger {

    public int feedbackId;
    public int newPriority;

    public FeedbackChanger() {
    }

    public FeedbackChanger(int feedbackId, int newPriority) {

        this.feedbackId = feedbackId;
        this.newPriority = newPriority;

    }

    public int getFeedbackId() {
        return feedbackId;
    }

    public void setFeedbackId(int feedbackId) {
        this.feedbackId = feedbackId;
    }

    public int getNewPriority() {
        return newPriority;
    }

    public void setNewPriority(int newPriority) {
        this.newPriority = newPriority;
    }

}
